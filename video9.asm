.import source "constants.asm"
.import source "vic2constants.asm"
BasicUpstart2(start)
*=$1000

start:
    sei
    lda 1
    and #251
    sta 1

    ldx #8
char_init_loop:
    lda VIC_BASE,x
    sta $3000,x
    inx
    bne char_init_loop

    lda 1
    ora #4
    sta 1

    //Enable enhanced mode
    lda SCREEN_CONTROL_REG
    ora #64
    sta SCREEN_CONTROL_REG
    cli

    lda #black
    sta BORDER_COLOR
    sta SCREEN_COLOR
    //enhanced mode colours
    lda #red
    sta TXT_COLOUR_1
    lda #blue
    sta TXT_COLOUR_2
    lda #yellow
    sta TXT_COLOUR_3

    jsr CLS
    lda GRAPHICS_POINTER
    and #240
    clc
    adc #12
    sta GRAPHICS_POINTER

    //line 1
    ldx #0
charloop:
    lda #0
    sta SCREEN_MEM,x
    lda #white
    sta COLOUR_MEM, x
    inx
    cpx #40
    bne charloop

    //line 2
    ldx #0
charloop2:
    lda #64
    sta SCREEN_MEM+40,x
    lda #yellow
    sta COLOUR_MEM+40, x
    inx
    cpx #40
    bne charloop2
    //line3
    ldx #0
charloop3:
    lda #128
    sta SCREEN_MEM+80,x
    lda #lblue
    sta COLOUR_MEM+80, x
    inx
    cpx #40
    bne charloop3
    //line4
    ldx #0
charloop4:
    lda #192
    sta SCREEN_MEM+120,x
    lda #red
    sta COLOUR_MEM+120, x
    inx
    cpx #40
    bne charloop4

    rts

*=$3000
.byte 255,17,255,68,255,17,255,68
/*
**********
*11111111* 255
*00010001* 17
*11111111* 255
*01000100* 68
*11111111* 255
*00010001* 17
*11111111* 255
*01000100* 68
**********
*/