.import source "constants.asm"
.import source "vic2constants.asm"

BasicUpstart2(irqinit)
*=$1000
spr0_direction:
.byte 00
spr_0_shadow_x:
.byte 00,00
spr_0_shadow_y:
.byte 00
irqinit:
        sei
        lda #%01111111
        sta INTERRUPT_REG
        lda RASTER_SPRITE_INT_REG
        ora #%00000001
        sta RASTER_SPRITE_INT_REG
        lda RASTER_LINE_MSB
        and #%01111111
        sta RASTER_LINE_MSB
        lda #0
        sta RASTER_LINE
        lda #<irq1
        sta INTERRUPT_EXECUTION_LOW
        lda #>irq1
        sta INTERRUPT_EXECUTION_HIGH
        cli
start:
    lda #200
    sta SPRITE_POINTER_0
    lda #yellow
    sta SPRITE_MULTICOLOR_3_0
    lda #44
    sta spr_0_shadow_x
    lda #120
    sta spr_0_shadow_y
    lda #%00000000
    sta SPRITE_HIRES

    lda #%00000000
    sta SPRITE_DOUBLE_X
    sta SPRITE_DOUBLE_Y

    lda #%00000001
    sta SPRITE_ENABLE
    lda #blue
    sta BORDER_COLOR
    lda #black
    sta SCREEN_COLOR
    jsr CLS
    jsr game_code
    rts
game_code:
testjoy:
joy_up:
    lda JPORT2
    and #%00000001
    bne joy_down
    lda #8
    sta spr0_direction
    dec spr_0_shadow_y

joy_down:
    lda JPORT2
    and #%00000010
    bne joy_left
    lda #12
    sta spr0_direction
    inc spr_0_shadow_y

joy_left:
    lda JPORT2
    and #%00000100
    bne joy_right
    lda #4
    sta spr0_direction
    sec
    lda spr_0_shadow_x
    sbc #1
    sta spr_0_shadow_x
    bcs joy_right
    lda #0
    sta spr_0_shadow_x + 1

joy_right:
    lda JPORT2
    and #%00001000
    bne check_borders
    lda #0
    sta spr0_direction
    clc
    lda spr_0_shadow_x
    adc #1
    sta spr_0_shadow_x
    bcc check_borders
    lda #1
    sta spr_0_shadow_x + 1

check_borders:
border_top:
    lda spr_0_shadow_y
    cmp #49
    bne border_lower
    lda #50
    sta spr_0_shadow_y
    jmp border_left

border_lower:
    lda spr_0_shadow_y
    cmp #232
    bne border_left
    lda #231
    sta spr_0_shadow_y

border_left:
    lda spr_0_shadow_x + 1
    cmp #0
    bne border_right
    lda spr_0_shadow_x
    cmp #21
    bne redraw_sprites
    lda #22
    sta spr_0_shadow_x

border_right:
    lda spr_0_shadow_x
    cmp #66
    bne redraw_sprites
    lda #65
    sta spr_0_shadow_x

redraw_sprites:
    lda RASTER_LINE
    cmp #$ff
    bne redraw_sprites
    lda spr_0_shadow_x
    sta SPRITE_0_X
    lda spr_0_shadow_y
    sta SPRITE_0_Y
    lda spr_0_shadow_x + 1
    cmp #1
    bne sprite_0_in_left
    lda SPRITE_MSB_X
    ora #%00000001
    jmp update_sprite_0_msb
sprite_0_in_left:
    lda SPRITE_MSB_X
    and #%11111110
update_sprite_0_msb:
    sta SPRITE_MSB_X
    jmp testjoy
quit:    
    rts
irq1:
        pha
        txa
        pha
        tya
        pha
        jsr sprite_0_animation 
        jmp ack

ack:
        dec INTERRUPT_STATUS

        pla
        tay  
        pla
        tax 
        pla
        jmp SYS_IRQ_HANDLER

sprite_0_animation:
        inc spr0_animation_delay_counter
        lda spr0_animation_delay_counter
        cmp #6
        bne spr0_no_action
        lda #00
        sta spr0_animation_delay_counter
        ldx spr0_frame_counter
        cpx #4
        bne spr0_continue
        ldx #0
        stx spr0_frame_counter
spr0_continue:
        clc
        txa
        adc spr0_direction
        tax
        lda spr0_anim_frames, x
        sta SPRITE_POINTER_0
        inc spr0_frame_counter
spr0_no_action:
        rts
spr0_anim_frames:
// default - right
.byte 200,201,202,201
// left
.byte 203,204,205,204
// up
.byte 206,207,208,207
// down
.byte 209,210,211,210
spr0_frame_counter:
.byte 00
spr0_animation_delay_counter:
.byte 00  

*=12800
sprite_0_r_0:
.byte $00,$00,$00,$00,$7e,$00,$01,$ff
.byte $80,$03,$ff,$c0,$07,$ff,$e0,$0f
.byte $fc,$f0,$0f,$fc,$f0,$1f,$ff,$f8
.byte $1f,$ff,$f8,$1f,$ff,$f8,$1f,$ff
.byte $f8,$1f,$ff,$f8,$1f,$ff,$f8,$0f
.byte $ff,$f0,$0f,$ff,$f0,$07,$ff,$e0
.byte $03,$ff,$c0,$01,$ff,$80,$00,$7e
.byte $00,$00,$00,$00,$00,$00,$00,$07

// sprite 1 / singlecolor / color: $07
sprite_0_r_1:
.byte $00,$00,$00,$00,$7e,$00,$01,$ff
.byte $80,$03,$ff,$c0,$07,$ff,$e0,$0f
.byte $fc,$f0,$0f,$fc,$f0,$1f,$ff,$f8
.byte $1f,$ff,$f0,$1f,$ff,$00,$1f,$f0
.byte $00,$1f,$ff,$00,$1f,$ff,$f0,$0f
.byte $ff,$f0,$0f,$ff,$f0,$07,$ff,$e0
.byte $03,$ff,$c0,$01,$ff,$80,$00,$7e
.byte $00,$00,$00,$00,$00,$00,$00,$07

// sprite 2 / singlecolor / color: $07
sprite_0_r_2:
.byte $00,$00,$00,$00,$7e,$00,$01,$ff
.byte $80,$03,$ff,$c0,$07,$ff,$e0,$0f
.byte $fc,$f0,$0f,$fc,$f0,$1f,$ff,$c0
.byte $1f,$ff,$00,$1f,$fc,$00,$1f,$f0
.byte $00,$1f,$fc,$00,$1f,$ff,$00,$0f
.byte $ff,$c0,$0f,$ff,$f0,$07,$ff,$e0
.byte $03,$ff,$c0,$01,$ff,$80,$00,$7e
.byte $00,$00,$00,$00,$00,$00,$00,$07

// sprite 3 / singlecolor / color: $07
sprite_0_l_0:
.byte $00,$00,$00,$00,$7e,$00,$01,$ff
.byte $80,$03,$ff,$c0,$07,$ff,$e0,$0f
.byte $3f,$f0,$0f,$3f,$f0,$1f,$ff,$f8
.byte $1f,$ff,$f8,$1f,$ff,$f8,$1f,$ff
.byte $f8,$1f,$ff,$f8,$1f,$ff,$f8,$0f
.byte $ff,$f0,$0f,$ff,$f0,$07,$ff,$e0
.byte $03,$ff,$c0,$01,$ff,$80,$00,$7e
.byte $00,$00,$00,$00,$00,$00,$00,$07

// sprite 4 / singlecolor / color: $07
sprite_0_l_1:
.byte $00,$00,$00,$00,$7e,$00,$01,$ff
.byte $80,$03,$ff,$c0,$07,$ff,$e0,$0f
.byte $3f,$f0,$0f,$3f,$f0,$1f,$ff,$f8
.byte $0f,$ff,$f8,$00,$ff,$f8,$00,$0f
.byte $f8,$00,$ff,$f8,$0f,$ff,$f8,$0f
.byte $ff,$f0,$0f,$ff,$f0,$07,$ff,$e0
.byte $03,$ff,$c0,$01,$ff,$80,$00,$7e
.byte $00,$00,$00,$00,$00,$00,$00,$07

// sprite 5 / singlecolor / color: $07
sprite_0_l_2:
.byte $00,$00,$00,$00,$7e,$00,$01,$ff
.byte $80,$03,$ff,$c0,$07,$ff,$e0,$0f
.byte $3f,$f0,$0f,$3f,$f0,$03,$ff,$f8
.byte $00,$ff,$f8,$00,$3f,$f8,$00,$0f
.byte $f8,$00,$3f,$f8,$00,$ff,$f8,$03
.byte $ff,$f0,$0f,$ff,$f0,$07,$ff,$e0
.byte $03,$ff,$c0,$01,$ff,$80,$00,$7e
.byte $00,$00,$00,$00,$00,$00,$00,$07

sprite_0_u_0:
.byte $00,$00,$00,$00,$7e,$00,$01,$ff
.byte $80,$03,$ff,$c0,$07,$ff,$e0,$0f
.byte $ff,$f0,$0f,$fe,$70,$1f,$fe,$78
.byte $1f,$ff,$f8,$1f,$ff,$f8,$1f,$ff
.byte $f8,$1f,$ff,$f8,$1f,$ff,$f8,$0f
.byte $ff,$f0,$0f,$ff,$f0,$07,$ff,$e0
.byte $03,$ff,$c0,$01,$ff,$80,$00,$7e
.byte $00,$00,$00,$00,$00,$00,$00,$07

// sprite 7 / singlecolor / color: $07
sprite_0_u_1:
.byte $00,$00,$00,$00,$02,$00,$01,$c7
.byte $80,$03,$c7,$c0,$07,$c7,$e0,$0f
.byte $c7,$f0,$0f,$ee,$70,$1f,$ee,$78
.byte $1f,$ef,$f8,$1f,$ef,$f8,$1f,$ff
.byte $f8,$1f,$ff,$f8,$1f,$ff,$f8,$0f
.byte $ff,$f0,$0f,$ff,$f0,$07,$ff,$e0
.byte $03,$ff,$c0,$01,$ff,$80,$00,$7e
.byte $00,$00,$00,$00,$00,$00,$00,$07

// sprite 8 / singlecolor / color: $07
sprite_0_u_2:
.byte $00,$00,$00,$00,$00,$00,$01,$01
.byte $80,$03,$01,$c0,$07,$83,$e0,$0f
.byte $83,$f0,$0f,$c6,$70,$1f,$c6,$78
.byte $1f,$ef,$f8,$1f,$ef,$f8,$1f,$ff
.byte $f8,$1f,$ff,$f8,$1f,$ff,$f8,$0f
.byte $ff,$f0,$0f,$ff,$f0,$07,$ff,$e0
.byte $03,$ff,$c0,$01,$ff,$80,$00,$7e
.byte $00,$00,$00,$00,$00,$00,$00,$07

// sprite 9 / singlecolor / color: $07
sprite_0_d_0:
.byte $00,$00,$00,$00,$7e,$00,$01,$ff
.byte $80,$03,$ff,$c0,$07,$ff,$e0,$0f
.byte $ff,$f0,$0f,$ff,$f0,$1f,$ff,$f8
.byte $1f,$ff,$f8,$1f,$ff,$f8,$1f,$ff
.byte $f8,$1f,$ff,$f8,$1f,$fe,$78,$0f
.byte $fe,$70,$0f,$ff,$f0,$07,$ff,$e0
.byte $03,$ff,$c0,$01,$ff,$80,$00,$7e
.byte $00,$00,$00,$00,$00,$00,$00,$07

// sprite 10 / singlecolor / color: $07
sprite_0_d_1:
.byte $00,$00,$00,$00,$7e,$00,$01,$ff
.byte $80,$03,$ff,$c0,$07,$ff,$e0,$0f
.byte $ff,$f0,$0f,$ff,$f0,$1f,$ff,$f8
.byte $1f,$ff,$f8,$1f,$ff,$f8,$1f,$ef
.byte $f8,$1f,$ef,$f8,$1f,$ee,$78,$0f
.byte $ee,$70,$0f,$c7,$f0,$07,$c7,$e0
.byte $03,$c7,$c0,$01,$c7,$80,$00,$02
.byte $00,$00,$00,$00,$00,$00,$00,$07

// sprite 11 / singlecolor / color: $07
sprite_0_d_2:
.byte $00,$00,$00,$00,$7e,$00,$01,$ff
.byte $80,$03,$ff,$c0,$07,$ff,$e0,$0f
.byte $ff,$f0,$0f,$ff,$f0,$1f,$ff,$f8
.byte $1f,$ff,$f8,$1f,$ff,$f8,$1f,$ef
.byte $f8,$1f,$ef,$f8,$1f,$c6,$78,$0f
.byte $c6,$70,$0f,$83,$f0,$07,$83,$e0
.byte $03,$01,$c0,$01,$01,$80,$00,$00
.byte $00,$00,$00,$00,$00,$00,$00,$07