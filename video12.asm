.import source "constants.asm"
.import source "vic2constants.asm"
BasicUpstart2(start)
*=$1000

start:
    sei
    //Turn off Interupts
    lda #$7f
    sta INTERRUPT_REG
    sta INTERRUPT_REG2
    // Multicolour mode
    lda VIC_CONTR_REG
    ora #16
    sta VIC_CONTR_REG
    // Turn off BASIC and Kernal
    lda #%00110101
    sta $01
    // Choose VIC Bank (high address - $c000)
    lda #0
    sta VIC_BANK_POINTER
    .label SCREEN_RAM=$c000
    // Point Custom Character Ram to VIC + $800 ($c800)
    lda #%00000010
    sta GRAPHICS_POINTER
    // Set to 38 Column Mode
    lda VIC_CONTR_REG
    and #%11110111
    sta VIC_CONTR_REG
    // Set up colours
    lda #black
    sta BORDER_COLOR
    lda #cyan
    sta SCREEN_COLOR
    lda #red
    sta TXT_COLOUR_1
    lda #yellow
    sta TXT_COLOUR_2
    cli
    jsr DrawMap
    !loop:
        lda #50
        cmp RASTER_LINE
        bne *-3
        lda FrameNumber
        cmp #2
        bne !DoNothing+
        lda #00
        sta FrameNumber
        lda MapPosition + 0
        sec
        sbc #1
        sta MapPosition + 0
        bcs !NoShifting+
        adc #08
        sta MapPosition + 0
        inc MapPosition + 1
        //dec MapPosition
        ldx MapPosition + 1
        jsr ShiftMapRight
        //jsr ShiftMapLeft
    !NoShifting:
        lda MapPosition + 0
        ora #%00010000
        sta VIC_CONTR_REG
    !DoNothing:
    inc FrameNumber
        jsr !loop-

MapPosition:
    .byte 00, 00
FrameNumber:
    .byte 00

ShiftMapRight:
    txa
    adc #40
    tax
    .for(var i=0; i<20; i++) {
        .for(var j=0; j<40; j++) {
            lda SCREEN_RAM + 40 * i + j + 1
            sta SCREEN_RAM + 40 * i + j + 0
            tay
            lda char_attrib_data, y
            sta COLOUR_MEM + 40 * i + j + 0
        }
        lda themap + 256 * i, x
        sta SCREEN_RAM + 40 * i + 39
        tay
        lda char_attrib_data, y
        sta COLOUR_MEM + 40 * i + 39

    }
    rts

ShiftMapLeft:
    .for(var i=0; i<20; i++) {
        lda themap + 256 * i, x
        sta SCREEN_RAM + 40 * i 
        tay
        lda char_attrib_data, y
        sta COLOUR_MEM + 40 * i
        .for(var j=40; j>=0; j--) {
            lda SCREEN_RAM + 40 * i + j + 0
            sta SCREEN_RAM + 40 * i + j + 1
            tay
            lda char_attrib_data, y
            sta COLOUR_MEM + 40 * i + j + 1
        }
        

    }
    rts


DrawMap: {
    ldx #0
    lda #<themap
    sta SelfModMap + 1
    lda #>themap
    sta SelfModMap + 2
    lda #<SCREEN_RAM
    sta SelfModScreen + 1
    lda #>SCREEN_RAM
    sta SelfModScreen + 2
    lda #<COLOUR_MEM
    sta SelfModColour + 1
    lda #>COLOUR_MEM
    sta SelfModColour + 2
    lda #25
    sta $ff
    !OuterLoop:
        ldx #39
        !InnerLoop:
            SelfModMap:
                lda $BEEF,x
            SelfModScreen:
                sta $BEEF,x
                tay
                lda char_attrib_data, y
            SelfModColour:
                sta $BEEF,x
            dex
            bpl !InnerLoop-
            clc
            lda SelfModScreen + 1
            adc #40
            sta SelfModScreen + 1
            sta SelfModColour + 1
            bcc !CarryClear+
            inc SelfModScreen + 2
            inc SelfModColour + 2
            !CarryClear:
            inc SelfModMap + 2
            dec $ff
            bne !OuterLoop-
            rts
}


    rts

    *=$8000
    themap:
        .import binary "./assets/vid11-map.bin"

    char_attrib_data:
    .byte $0D,$0A,$0A,$0A,$0D,$09,$09,$09,$09,$09,$0D,$09,$09,$09,$09,$0D
    .byte $0D,$0E

    // Custom Chars
    *=$c800
    .byte $00,$00,$00,$00,$00,$00,$00,$00,$AA,$95,$95,$95,$95,$95,$95,$95
    .byte $AA,$56,$56,$56,$56,$56,$56,$56,$AA,$55,$55,$55,$55,$55,$55,$55
    .byte $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF
    .byte $00,$03,$0F,$3F,$3F,$FF,$FF,$FF,$00,$C0,$F0,$FC,$FC,$FF,$FF,$FF
    .byte $FF,$FF,$FF,$3F,$3F,$0F,$03,$00,$FF,$FF,$FF,$FC,$FC,$F0,$C0,$00
    .byte $AA,$AA,$AA,$AA,$AA,$AA,$AA,$AA,$00,$02,$0A,$2A,$2A,$AA,$AA,$AA
    .byte $00,$80,$A0,$A8,$A8,$AA,$AA,$AA,$AA,$AA,$AA,$2A,$2A,$0A,$02,$00
    .byte $AA,$AA,$AA,$A8,$A8,$A0,$80,$00,$00,$02,$02,$02,$02,$02,$02,$00
    .byte $00,$80,$80,$80,$80,$80,$80,$00,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF