.import source "constants.asm"
.import source "vic2constants.asm"

BasicUpstart2(irqinit)
*=$1000
spr0_direction:
.byte 00

irqinit:
        sei
        lda #%01111111
        sta INTERRUPT_REG
        lda RASTER_SPRITE_INT_REG
        ora #%00000001
        sta RASTER_SPRITE_INT_REG
        lda RASTER_LINE_MSB
        and #%01111111
        sta RASTER_LINE_MSB
        lda #0
        sta RASTER_LINE
        lda #<irq1
        sta INTERRUPT_EXECUTION_LOW
        lda #>irq1
        sta INTERRUPT_EXECUTION_HIGH
        cli
start:
    lda #200
    sta SPRITE_POINTER_0
    lda #yellow
    sta SPRITE_MULTICOLOR_3_0
    lda #44
    sta SPRITE_0_X
    lda #120
    sta SPRITE_0_Y
    lda #%00000000
    sta SPRITE_HIRES

    lda #%00000001
    sta SPRITE_DOUBLE_X
    sta SPRITE_DOUBLE_Y

    lda #%00000001
    sta SPRITE_ENABLE
    lda #blue
    sta BORDER_COLOR
    sta SCREEN_COLOR
    jsr CLS
    jsr game_code
    rts
game_code:
keyloop:
    jsr CHR_IN
    cmp #$103
    beq quit
    cmp #$4c
    beq left
    cmp #$52
    beq right
    jmp keyloop
left:
    lda #4
    sta spr0_direction
    jmp keyloop
right:
    lda #0
    sta spr0_direction
    jmp keyloop
quit:    
    rts
irq1:
        pha
        txa
        pha
        tya
        pha
        jsr sprite_0_animation 
        jmp ack

ack:
        dec INTERRUPT_STATUS

        pla
        tay  
        pla
        tax 
        pla
        jmp SYS_IRQ_HANDLER

sprite_0_animation:
        inc spr0_animation_delay_counter
        lda spr0_animation_delay_counter
        cmp #6
        bne spr0_no_action
        lda #00
        sta spr0_animation_delay_counter
        ldx spr0_frame_counter
        cpx #4
        bne spr0_continue
        ldx #0
        stx spr0_frame_counter
spr0_continue:
        clc
        txa
        adc spr0_direction
        tax
        lda spr0_anim_frames, x
        sta SPRITE_POINTER_0
        inc spr0_frame_counter
spr0_no_action:
        rts
spr0_anim_frames:
// default - right
.byte 200,201,202,201
// left
.byte 203,204,205,204
spr0_frame_counter:
.byte 00
spr0_animation_delay_counter:
.byte 00  

*=12800
sprite_0_r_0:
.byte $00,$00,$00,$00,$7e,$00,$01,$ff
.byte $80,$03,$ff,$c0,$07,$ff,$e0,$0f
.byte $fc,$f0,$0f,$fc,$f0,$1f,$ff,$f8
.byte $1f,$ff,$f8,$1f,$ff,$f8,$1f,$ff
.byte $f8,$1f,$ff,$f8,$1f,$ff,$f8,$0f
.byte $ff,$f0,$0f,$ff,$f0,$07,$ff,$e0
.byte $03,$ff,$c0,$01,$ff,$80,$00,$7e
.byte $00,$00,$00,$00,$00,$00,$00,$07

// sprite 1 / singlecolor / color: $07
sprite_0_r_1:
.byte $00,$00,$00,$00,$7e,$00,$01,$ff
.byte $80,$03,$ff,$c0,$07,$ff,$e0,$0f
.byte $fc,$f0,$0f,$fc,$f0,$1f,$ff,$f8
.byte $1f,$ff,$f0,$1f,$ff,$00,$1f,$f0
.byte $00,$1f,$ff,$00,$1f,$ff,$f0,$0f
.byte $ff,$f0,$0f,$ff,$f0,$07,$ff,$e0
.byte $03,$ff,$c0,$01,$ff,$80,$00,$7e
.byte $00,$00,$00,$00,$00,$00,$00,$07

// sprite 2 / singlecolor / color: $07
sprite_0_r_2:
.byte $00,$00,$00,$00,$7e,$00,$01,$ff
.byte $80,$03,$ff,$c0,$07,$ff,$e0,$0f
.byte $fc,$f0,$0f,$fc,$f0,$1f,$ff,$c0
.byte $1f,$ff,$00,$1f,$fc,$00,$1f,$f0
.byte $00,$1f,$fc,$00,$1f,$ff,$00,$0f
.byte $ff,$c0,$0f,$ff,$f0,$07,$ff,$e0
.byte $03,$ff,$c0,$01,$ff,$80,$00,$7e
.byte $00,$00,$00,$00,$00,$00,$00,$07

// sprite 3 / singlecolor / color: $07
sprite_0_l_0:
.byte $00,$00,$00,$00,$7e,$00,$01,$ff
.byte $80,$03,$ff,$c0,$07,$ff,$e0,$0f
.byte $3f,$f0,$0f,$3f,$f0,$1f,$ff,$f8
.byte $1f,$ff,$f8,$1f,$ff,$f8,$1f,$ff
.byte $f8,$1f,$ff,$f8,$1f,$ff,$f8,$0f
.byte $ff,$f0,$0f,$ff,$f0,$07,$ff,$e0
.byte $03,$ff,$c0,$01,$ff,$80,$00,$7e
.byte $00,$00,$00,$00,$00,$00,$00,$07

// sprite 4 / singlecolor / color: $07
sprite_0_l_1:
.byte $00,$00,$00,$00,$7e,$00,$01,$ff
.byte $80,$03,$ff,$c0,$07,$ff,$e0,$0f
.byte $3f,$f0,$0f,$3f,$f0,$1f,$ff,$f8
.byte $0f,$ff,$f8,$00,$ff,$f8,$00,$0f
.byte $f8,$00,$ff,$f8,$0f,$ff,$f8,$0f
.byte $ff,$f0,$0f,$ff,$f0,$07,$ff,$e0
.byte $03,$ff,$c0,$01,$ff,$80,$00,$7e
.byte $00,$00,$00,$00,$00,$00,$00,$07

// sprite 5 / singlecolor / color: $07
sprite_0_l_2:
.byte $00,$00,$00,$00,$7e,$00,$01,$ff
.byte $80,$03,$ff,$c0,$07,$ff,$e0,$0f
.byte $3f,$f0,$0f,$3f,$f0,$03,$ff,$f8
.byte $00,$ff,$f8,$00,$3f,$f8,$00,$0f
.byte $f8,$00,$3f,$f8,$00,$ff,$f8,$03
.byte $ff,$f0,$0f,$ff,$f0,$07,$ff,$e0
.byte $03,$ff,$c0,$01,$ff,$80,$00,$7e
.byte $00,$00,$00,$00,$00,$00,$00,$07