.import source "constants.asm"
.import source "vic2constants.asm"

BasicUpstart2(start)
*=$1000
start:
    lda #200
    sta SPRITE_POINTER_0
    lda #201
    sta SPRITE_POINTER_1
    lda #black
    sta SPRITE_MULTICOLOR_3_0
    lda #yellow
    sta SPRITE_MULTICOLOR_3_1
    lda #cyan
    sta SPRITE_MULTICOLOR_1
    lda #red
    sta SPRITE_MULTICOLOR_2
    lda #44
    sta SPRITE_0_X
    sta SPRITE_1_X
    lda #120
    sta SPRITE_0_Y
    sta SPRITE_1_Y
    lda #%00000010
    sta SPRITE_HIRES

    lda #%00000011
    sta SPRITE_DOUBLE_X
    sta SPRITE_DOUBLE_Y

    lda #%00000011
    sta SPRITE_ENABLE
    lda #blue
    sta BORDER_COLOR
    sta SCREEN_COLOR
    jsr CLS
    rts

*=12800
sprite_0:
.byte $00,$00,$00,$03,$ff,$80,$07,$ff
.byte $f0,$0e,$0f,$f8,$0e,$00,$1c,$7c
.byte $00,$0f,$d8,$80,$07,$d9,$c3,$83
.byte $52,$42,$c2,$52,$66,$42,$63,$66
.byte $42,$21,$c3,$c2,$20,$00,$02,$20
.byte $00,$06,$27,$00,$cc,$27,$ff,$88
.byte $31,$81,$88,$18,$f3,$30,$0f,$1e
.byte $60,$01,$ff,$c0,$00,$00,$00,$00
sprite_1:
.byte $00,$00,$00,$00,$00,$00,$00,$00
.byte $00,$02,$a8,$00,$02,$aa,$a0,$0a
.byte $aa,$a0,$2a,$aa,$a8,$2a,$aa,$a8
.byte $29,$69,$a8,$29,$69,$68,$2a,$69
.byte $68,$2a,$aa,$a8,$2a,$be,$a8,$2a
.byte $be,$a8,$2a,$aa,$a0,$2a,$aa,$a0
.byte $0a,$ff,$a0,$0a,$ae,$80,$00,$aa
.byte $80,$00,$00,$00,$00,$00,$00,$87