.import source "constants.asm"
.import source "vic2constants.asm"
BasicUpstart2(start)
*=$1000

start:
    sei
    lda 1
    and #251
    sta 1
    ldx #8
char_init_loop:
    lda VIC_BASE,x
    sta $3000,x
    inx
    bne char_init_loop
    lda 1
    ora #4
    sta 1
    cli

    lda #black
    sta BORDER_COLOR
    sta SCREEN_COLOR
    lda #white
    sta TEXT_COLOR
    jsr CLS
    lda GRAPHICS_POINTER
    and #240
    clc
    adc #12
    sta GRAPHICS_POINTER

    ldx #40
charloop:
    lda #0
    dex
    sta SCREEN_MEM,x
    bne charloop
    rts

*=$3000
.byte 60,66,165,129,165,153,66,60
