.import source "constants.asm"
.import source "vic2constants.asm"
BasicUpstart2(start)
*=$1000

start:
    sei
    lda 1
    and #251
    sta 1
    ldx #8
char_init_loop:
    lda VIC_BASE,x
    sta $3000,x
    inx
    bne char_init_loop
    lda 1
    ora #4
    sta 1
    // Enable multicolour mode
    lda VIC_CONTR_REG
    ora #16
    sta VIC_CONTR_REG
    cli

    lda #black
    sta BORDER_COLOR
    sta SCREEN_COLOR
    lda #lred
    sta TEXT_COLOR
    lda #yellow
    sta TXT_COLOUR_1
    lda #blue
    sta TXT_COLOUR_2
    jsr CLS

    // Point char bank at $3000
    lda GRAPHICS_POINTER
    and #240
    clc
    adc #12
    sta GRAPHICS_POINTER

    // draw a line of character 0
    ldx #40
charloop:
    lda #0
    dex
    sta SCREEN_MEM,x
    bne charloop
    rts

// Character Definition
*=$3000
/*
00 = background
01 = text colour 1
10 = text colour 2
11 = foreground
* * * *
11 11 11 11 = 255
01 01 01 01 = 85
10 10 10 10 = 170
*/
.byte 255,255,255,170,170,85,85,85